package br.ita.airgroup.icsr.multirobotnetworks.gui;

import java.awt.BorderLayout;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import br.ita.airgroup.icsr.multirobotnetworks.environment.Scenario;

public class ScenarioWindow extends JFrame{
	
	private static final long serialVersionUID = 5946807408156710953L;
	private Scenario scenario;
	private ScenarioPanel panel;

	public ScenarioWindow(Scenario scenario) {
		super(scenario.getName());
		this.scenario = scenario;
		
		createGUI();
	}

	private void createGUI() {
		
		this.panel = new ScenarioPanel(scenario);
		this.setLayout(new BorderLayout());
		
		this.add(panel, BorderLayout.CENTER);
		
		JPanel p = new JPanel();
		p.add(new JLabel("teste tewste g "));
		
		this.add(p, BorderLayout.NORTH);
		
		this.setSize(800, 600);
		
		this.panel.draw();
		
		this.setVisible(true);
		
	}
	
}
