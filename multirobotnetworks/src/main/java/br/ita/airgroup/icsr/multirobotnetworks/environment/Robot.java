package br.ita.airgroup.icsr.multirobotnetworks.environment;

public class Robot {
	
	private double x;
	private double y;
	private double range;
	private double bodyRadius;
	
	public Robot(){
		
	}
	
	public Robot(double x, double y, double range) {
		super();
		this.x = x;
		this.y = y;
		this.range = range;
		this.bodyRadius = range/50;
	}

	public double getX() {
		return x;
	}

	public void setX(double x) {
		this.x = x;
	}

	public double getY() {
		return y;
	}

	public void setY(double y) {
		this.y = y;
	}

	public double getRange() {
		return range;
	}

	public void setRange(double range) {
		this.range = range;
	}

	public double getBodyRadius() {
		return bodyRadius;
	}

	public void setBodyRadius(double bodyRadius) {
		this.bodyRadius = bodyRadius;
	}
	
	

}
