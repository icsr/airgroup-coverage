package br.ita.airgroup.icsr.multirobotnetworks.gui;

import java.awt.Color;
import java.awt.Graphics;

import javax.swing.JPanel;

import br.ita.airgroup.icsr.multirobotnetworks.environment.Robot;
import br.ita.airgroup.icsr.multirobotnetworks.environment.Scenario;

public class ScenarioPanel extends JPanel{

	private static final long serialVersionUID = 1L;
	
	private Scenario scenario;
	
	public ScenarioPanel(Scenario scenario) {
		super(true);
		this.scenario = scenario;		
		this.setSize(scenario.getDimension());	
		this.setVisible(true);
	}



	@Override
	public void paint(Graphics g) {	
					
		g.setColor(Color.WHITE);
		g.fillRect(0, 0, getWidth(), getHeight());
		
		g.setColor(Color.BLACK);
		
		for(Robot r : scenario.getNetwork()){
			int[] coords = translate(r.getX(), r.getY(), r.getRange());
			g.drawOval(coords[0], coords[1], coords[2], coords[2]);
			
			coords = translate(r.getX(), r.getY(), r.getBodyRadius());
			g.fillOval(coords[0], coords[1], coords[2], coords[2]);
		}
	
		
	}
	
	public int[] translate(double x, double y, double radius){
		return new int[]{ (int)(x - radius), (int)(y - radius), (int)(2*radius) };
	}



	public void draw() {		
		this.repaint();		
	}
	
	
	
	

}
