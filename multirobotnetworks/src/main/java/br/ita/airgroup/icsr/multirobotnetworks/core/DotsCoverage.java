package br.ita.airgroup.icsr.multirobotnetworks.core;

import br.ita.airgroup.icsr.multirobotnetworks.environment.Robot;
import br.ita.airgroup.icsr.multirobotnetworks.environment.Scenario;

public class DotsCoverage extends Coverage {

	public DotsCoverage(Scenario scenario) {
		super(scenario);
	}
	
	@Override
	public double calculate(){
		
		double total = 0;
		double covered = 0;
		
		for(int i = 0; i < scenario.getDimension().getWidth(); i++){
			for(int j = 0; j < scenario.getDimension().getHeight(); j++){
				
				total++;
				for(Robot r : scenario.getNetwork()){
					double distance = distance(i,j, r.getX(), r.getY());
					if(distance <= r.getRange() && distance >= r.getBodyRadius()){
						covered++;
						continue;
					}
					
					
				}
				
			}
				
			
		}
		
		return covered / total;
		
	}

}
