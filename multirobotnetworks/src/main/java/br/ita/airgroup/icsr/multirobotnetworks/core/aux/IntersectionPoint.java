package br.ita.airgroup.icsr.multirobotnetworks.core.aux;

import java.util.List;

import br.ita.airgroup.icsr.multirobotnetworks.environment.Robot;

public class IntersectionPoint {

	private double x, y;
	private List<Robot> robots;
	private Boolean internal = null;

	public IntersectionPoint(double x, double y, List<Robot> robots) {
		this.x = x;
		this.y = y;
		this.robots = robots;
	}

	public double getX() {
		return x;
	}

	public void setX(double x) {
		this.x = x;
	}

	public double getY() {
		return y;
	}

	public void setY(double y) {
		this.y = y;
	}

	public List<Robot> getRobots() {
		return robots;
	}

	public void setRobots(List<Robot> robots) {
		this.robots = robots;
	}

	public Boolean getInternal() {
		return internal;
	}

	public void setInternal(Boolean internal) {
		this.internal = internal;
	}

}
